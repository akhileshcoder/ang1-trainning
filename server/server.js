var express = require('express'),
    app = express();
const bodyParser = require('body-parser'),
    cors = require('cors');

app.use(cors());
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({extended: true,limit: '5mb'}));
app.use(express.static(__dirname + '/../client'));


app.get("/get-data",function (req,res) {
    var passedData = req.query;
    res.send({
        "msg":"query sent was: "+JSON.stringify(passedData),
        "nameList":['ram','aam','sita','gita','jorden','kaali']
    });
});
app.post("/get-data",function (req,res) {
    var passedData = req.body;
    res.send({
        "msg":"query sent was: "+JSON.stringify(passedData),
        "nameList":['ram','aam','sita','gita','jorden','kaali']
    });
});

var srvr=app.listen(5009, function() {
    console.log('listening on port : ' + 5009);
});
