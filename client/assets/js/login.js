
myAngApp.controller("login",["$scope","$http",function ($scope,$http) {

    $scope.myPageName = 'Dashboard';
    $scope.myTitle = "My APP tITLE";

    $http.get("/get-data?name=vivek")
        .then(function(response) {

            console.log("Messege from server is : ",response.data.msg);
            $scope.myList = response.data.nameList;

            console.log("response: ",response);
        });
    $http({
        method : "POST",
        url : "/get-data",
        data: { 'name' : 'Prashanthi' }
    }).then(function mySuccess(response) {
        console.log("Messege from server is : ",response.data.msg);
        $scope.dataFromPostReq = response.data.nameList;
    }, function myError(response) {
        $scope.myWelcome = response.statusText;
    });
}]);
